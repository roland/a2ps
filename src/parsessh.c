/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "parsessh.y"
 /* -*- c -*- */
/*
 * Grammar for parsing the style sheets
 *
 * Copyright (c) 1988-1993 Miguel Santana
 * Copyright (c) 1995-1999 Akim Demaille, Miguel Santana
 *
 */

/*
 * This file is part of a2ps
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * $Id: parsessh.y,v 1.1.1.1.2.2 2007/12/29 01:58:35 mhatta Exp $
 */

#include <config.h>

#include "a2ps.h"
#include "jobs.h"
#include "ffaces.h"
#include "ssheet.h"
#include "message.h"
#include "routines.h"
#include "yy2ssh.h"

#define YYDEBUG 1
#define YYERROR_VERBOSE 1
#define YYPRINT(file, type, value)   yyprint (file, type, value)

/* We need to use the same `const' as bison, to avoid the following
   prototypes to diverge from the function declarations */
#undef const
#ifndef __cplusplus
# ifndef __STDC__
#  define const
# endif
#endif

/* Comes from the parser */
extern int sshlineno;

/* Comes from the caller */
extern FILE * sshin;
extern struct a2ps_job * job;
extern const char * sshfilename;

/* Local prototypes */
void yyerror (const char *msg);
static void yyprint ();

/* Initilizes the obstacks */
void sshlex_initialize (void);

/* Comes from main.c */
extern int highlight_level;

int yylex (void);
struct style_sheet * parse_style_sheet (const char * filename);

/* Defines the style sheet being loaded */
static struct style_sheet * parsed_style_sheet = NULL;


#line 152 "parsessh.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_PARSESSH_H_INCLUDED
# define YY_YY_PARSESSH_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    tSTYLE = 258,                  /* tSTYLE  */
    tIS = 259,                     /* tIS  */
    tEND = 260,                    /* tEND  */
    tKEYWORDS = 261,               /* tKEYWORDS  */
    tARE = 262,                    /* tARE  */
    tIN = 263,                     /* tIN  */
    tOPERATORS = 264,              /* tOPERATORS  */
    tSEQUENCES = 265,              /* tSEQUENCES  */
    tFIRST = 266,                  /* tFIRST  */
    tSECOND = 267,                 /* tSECOND  */
    tALPHABET = 268,               /* tALPHABET  */
    tALPHABETS = 269,              /* tALPHABETS  */
    tDOCUMENTATION = 270,          /* tDOCUMENTATION  */
    tEXCEPTIONS = 271,             /* tEXCEPTIONS  */
    tCASE = 272,                   /* tCASE  */
    tCSTRING = 273,                /* tCSTRING  */
    tCCHAR = 274,                  /* tCCHAR  */
    tOPTIONAL = 275,               /* tOPTIONAL  */
    tCLOSERS = 276,                /* tCLOSERS  */
    tWRITTEN = 277,                /* tWRITTEN  */
    tBY = 278,                     /* tBY  */
    tVERSION = 279,                /* tVERSION  */
    tREQUIRES = 280,               /* tREQUIRES  */
    tA2PS = 281,                   /* tA2PS  */
    tANCESTORS = 282,              /* tANCESTORS  */
    tFACE = 283,                   /* tFACE  */
    tFFLAGS = 284,                 /* tFFLAGS  */
    tSTRING = 285,                 /* tSTRING  */
    tLATEXSYMBOL = 286,            /* tLATEXSYMBOL  */
    tREGEX = 287,                  /* tREGEX  */
    tSENSITIVENESS = 288,          /* tSENSITIVENESS  */
    tBACK_REF = 289                /* tBACK_REF  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define tSTYLE 258
#define tIS 259
#define tEND 260
#define tKEYWORDS 261
#define tARE 262
#define tIN 263
#define tOPERATORS 264
#define tSEQUENCES 265
#define tFIRST 266
#define tSECOND 267
#define tALPHABET 268
#define tALPHABETS 269
#define tDOCUMENTATION 270
#define tEXCEPTIONS 271
#define tCASE 272
#define tCSTRING 273
#define tCCHAR 274
#define tOPTIONAL 275
#define tCLOSERS 276
#define tWRITTEN 277
#define tBY 278
#define tVERSION 279
#define tREQUIRES 280
#define tA2PS 281
#define tANCESTORS 282
#define tFACE 283
#define tFFLAGS 284
#define tSTRING 285
#define tLATEXSYMBOL 286
#define tREGEX 287
#define tSENSITIVENESS 288
#define tBACK_REF 289

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 82 "parsessh.y"

  int integer;
  char * string;
  struct pattern * pattern;
  struct style_sheet * sheet;
  struct rule * rule;
  struct sequence * sequence;
  struct darray * array;
  struct words * words;
  struct faced_string * faced_string;
  enum face_e face;			/* Face			*/
  enum fflag_e fflags;			/* Flags for faces	*/
  struct fface_s fface;			/* Flagged face		*/
  enum case_sensitiveness sensitiveness;

#line 289 "parsessh.c"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;


int yyparse (void);


#endif /* !YY_YY_PARSESSH_H_INCLUDED  */
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_tSTYLE = 3,                     /* tSTYLE  */
  YYSYMBOL_tIS = 4,                        /* tIS  */
  YYSYMBOL_tEND = 5,                       /* tEND  */
  YYSYMBOL_tKEYWORDS = 6,                  /* tKEYWORDS  */
  YYSYMBOL_tARE = 7,                       /* tARE  */
  YYSYMBOL_tIN = 8,                        /* tIN  */
  YYSYMBOL_tOPERATORS = 9,                 /* tOPERATORS  */
  YYSYMBOL_tSEQUENCES = 10,                /* tSEQUENCES  */
  YYSYMBOL_tFIRST = 11,                    /* tFIRST  */
  YYSYMBOL_tSECOND = 12,                   /* tSECOND  */
  YYSYMBOL_tALPHABET = 13,                 /* tALPHABET  */
  YYSYMBOL_tALPHABETS = 14,                /* tALPHABETS  */
  YYSYMBOL_tDOCUMENTATION = 15,            /* tDOCUMENTATION  */
  YYSYMBOL_tEXCEPTIONS = 16,               /* tEXCEPTIONS  */
  YYSYMBOL_tCASE = 17,                     /* tCASE  */
  YYSYMBOL_tCSTRING = 18,                  /* tCSTRING  */
  YYSYMBOL_tCCHAR = 19,                    /* tCCHAR  */
  YYSYMBOL_tOPTIONAL = 20,                 /* tOPTIONAL  */
  YYSYMBOL_tCLOSERS = 21,                  /* tCLOSERS  */
  YYSYMBOL_tWRITTEN = 22,                  /* tWRITTEN  */
  YYSYMBOL_tBY = 23,                       /* tBY  */
  YYSYMBOL_tVERSION = 24,                  /* tVERSION  */
  YYSYMBOL_tREQUIRES = 25,                 /* tREQUIRES  */
  YYSYMBOL_tA2PS = 26,                     /* tA2PS  */
  YYSYMBOL_tANCESTORS = 27,                /* tANCESTORS  */
  YYSYMBOL_tFACE = 28,                     /* tFACE  */
  YYSYMBOL_tFFLAGS = 29,                   /* tFFLAGS  */
  YYSYMBOL_tSTRING = 30,                   /* tSTRING  */
  YYSYMBOL_tLATEXSYMBOL = 31,              /* tLATEXSYMBOL  */
  YYSYMBOL_tREGEX = 32,                    /* tREGEX  */
  YYSYMBOL_tSENSITIVENESS = 33,            /* tSENSITIVENESS  */
  YYSYMBOL_tBACK_REF = 34,                 /* tBACK_REF  */
  YYSYMBOL_35_ = 35,                       /* ','  */
  YYSYMBOL_36_ = 36,                       /* '('  */
  YYSYMBOL_37_ = 37,                       /* ')'  */
  YYSYMBOL_38_ = 38,                       /* '+'  */
  YYSYMBOL_YYACCEPT = 39,                  /* $accept  */
  YYSYMBOL_file = 40,                      /* file  */
  YYSYMBOL_style_sheet = 41,               /* style_sheet  */
  YYSYMBOL_definition_list = 42,           /* definition_list  */
  YYSYMBOL_requirement = 43,               /* requirement  */
  YYSYMBOL_documentation = 44,             /* documentation  */
  YYSYMBOL_long_string = 45,               /* long_string  */
  YYSYMBOL_authors = 46,                   /* authors  */
  YYSYMBOL_version = 47,                   /* version  */
  YYSYMBOL_ancestors_def = 48,             /* ancestors_def  */
  YYSYMBOL_ancestors_list = 49,            /* ancestors_list  */
  YYSYMBOL_case_def = 50,                  /* case_def  */
  YYSYMBOL_regex = 51,                     /* regex  */
  YYSYMBOL_rhs = 52,                       /* rhs  */
  YYSYMBOL_a_rhs = 53,                     /* a_rhs  */
  YYSYMBOL_rhs_list = 54,                  /* rhs_list  */
  YYSYMBOL_fface = 55,                     /* fface  */
  YYSYMBOL_fface_sxp = 56,                 /* fface_sxp  */
  YYSYMBOL_rule = 57,                      /* rule  */
  YYSYMBOL_keywords_def = 58,              /* keywords_def  */
  YYSYMBOL_keywords_rule_list = 59,        /* keywords_rule_list  */
  YYSYMBOL_keyword_regex = 60,             /* keyword_regex  */
  YYSYMBOL_operators_def = 61,             /* operators_def  */
  YYSYMBOL_operators_rule_list = 62,       /* operators_rule_list  */
  YYSYMBOL_operator_regex = 63,            /* operator_regex  */
  YYSYMBOL_sequence_def = 64,              /* sequence_def  */
  YYSYMBOL_sequence_list = 65,             /* sequence_list  */
  YYSYMBOL_sequence = 66,                  /* sequence  */
  YYSYMBOL_closers_opt = 67,               /* closers_opt  */
  YYSYMBOL_exception_def_opt = 68          /* exception_def_opt  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  5
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   218

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  39
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  30
/* YYNRULES -- Number of rules.  */
#define YYNRULES  92
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  200

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   289


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      36,    37,     2,    38,    35,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   130,   130,   137,   146,   149,   157,   163,   171,   177,
     182,   186,   190,   196,   201,   206,   209,   212,   215,   218,
     233,   234,   237,   239,   240,   253,   256,   257,   262,   269,
     277,   284,   292,   296,   307,   315,   319,   323,   327,   331,
     335,   342,   348,   359,   364,   371,   381,   386,   391,   397,
     411,   416,   422,   434,   438,   445,   450,   455,   460,   472,
     477,   484,   494,   498,   505,   511,   517,   522,   534,   539,
     545,   555,   558,   564,   587,   595,   603,   611,   619,   627,
     635,   643,   651,   659,   666,   674,   678,   686,   694,   700,
     706,   715,   718
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "tSTYLE", "tIS",
  "tEND", "tKEYWORDS", "tARE", "tIN", "tOPERATORS", "tSEQUENCES", "tFIRST",
  "tSECOND", "tALPHABET", "tALPHABETS", "tDOCUMENTATION", "tEXCEPTIONS",
  "tCASE", "tCSTRING", "tCCHAR", "tOPTIONAL", "tCLOSERS", "tWRITTEN",
  "tBY", "tVERSION", "tREQUIRES", "tA2PS", "tANCESTORS", "tFACE",
  "tFFLAGS", "tSTRING", "tLATEXSYMBOL", "tREGEX", "tSENSITIVENESS",
  "tBACK_REF", "','", "'('", "')'", "'+'", "$accept", "file",
  "style_sheet", "definition_list", "requirement", "documentation",
  "long_string", "authors", "version", "ancestors_def", "ancestors_list",
  "case_def", "regex", "rhs", "a_rhs", "rhs_list", "fface", "fface_sxp",
  "rule", "keywords_def", "keywords_rule_list", "keyword_regex",
  "operators_def", "operators_rule_list", "operator_regex", "sequence_def",
  "sequence_list", "sequence", "closers_opt", "exception_def_opt", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-150)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      11,    -2,    31,  -150,    39,  -150,  -150,    98,    66,   126,
     183,    56,    62,    65,    75,    91,    40,   175,    77,     7,
      80,   109,  -150,  -150,  -150,  -150,  -150,  -150,  -150,  -150,
      69,    60,   132,    94,    60,   133,    35,   131,   142,   140,
     167,  -150,   126,   183,  -150,   168,   169,  -150,     8,   170,
     129,  -150,    18,   120,  -150,     1,  -150,  -150,  -150,   154,
     189,  -150,    18,   120,  -150,     4,  -150,   194,  -150,  -150,
    -150,   138,    34,   143,    10,  -150,   172,   173,  -150,  -150,
       5,   198,   196,  -150,  -150,   176,  -150,  -150,    14,    60,
    -150,    60,  -150,  -150,  -150,   129,   120,  -150,  -150,  -150,
      69,  -150,  -150,   155,    69,   120,  -150,  -150,    94,    94,
      60,    60,   111,   129,   120,    60,   111,   197,    35,  -150,
    -150,   146,  -150,  -150,  -150,  -150,   181,   179,  -150,  -150,
    -150,    92,   118,  -150,  -150,  -150,   166,    21,   127,  -150,
    -150,    22,   111,    38,   183,   148,    38,  -150,  -150,   195,
     151,   152,   111,    38,   195,  -150,  -150,  -150,  -150,  -150,
     129,  -150,  -150,  -150,  -150,  -150,  -150,  -150,    38,   195,
     195,   191,   195,   183,  -150,    60,    60,    38,   195,   195,
    -150,  -150,   195,  -150,  -150,  -150,  -150,   199,    38,    38,
     195,  -150,  -150,  -150,  -150,   195,   195,  -150,  -150,  -150
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       0,     0,     0,     2,     0,     1,     4,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    19,    16,    17,    18,    11,    15,    10,     3,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    31,     0,     0,     9,     0,     0,    27,     0,     0,
      51,    32,     0,    60,    55,     0,    56,    43,    44,     0,
       0,     6,     0,    69,    64,     0,    65,     0,     8,    85,
      86,     0,     0,     0,     0,    72,     0,     0,    12,    23,
       0,     0,     0,    25,    26,     0,    20,    29,     0,    36,
      40,    39,    50,    34,    37,     0,     0,    33,    59,    54,
       0,    46,    47,     0,     0,     0,    68,    63,     0,     0,
       0,     0,    87,     0,     0,     0,    87,     0,     0,    13,
      14,     0,    24,     5,     7,    21,     0,     0,    35,    38,
      41,     0,     0,    57,    58,    45,     0,     0,     0,    66,
      67,     0,    87,    87,     0,     0,    87,    88,    89,    91,
       0,     0,    87,    87,    91,    71,    73,    22,    28,    30,
       0,    52,    61,    48,    49,    53,    70,    62,    87,    91,
      91,     0,    91,     0,    78,     0,     0,    87,    91,    91,
      82,    42,    91,    77,    74,    90,    76,     0,    87,    87,
      91,    81,    80,    75,    92,    91,    91,    79,    83,    84
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -150,  -150,  -150,  -150,  -150,  -150,  -150,  -150,  -150,  -150,
    -150,  -150,   -28,   -41,   -46,    23,   -31,  -150,   -17,   171,
     106,   114,   -42,   107,   -15,   200,  -150,   100,   -91,  -149
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_uint8 yydefgoto[] =
{
       0,     2,     3,     7,    22,    23,    80,    24,    25,    26,
      88,    27,    63,    92,   130,   131,    94,   103,   147,    32,
      55,    56,    35,    65,   148,    28,    74,    75,   149,   174
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      60,    82,    53,    67,    93,   180,    99,    93,    73,   107,
     121,    46,    98,    54,     1,   117,    64,    93,    66,   126,
     183,   184,   106,   186,    96,   154,   165,   167,     4,   191,
     192,     5,    85,   193,   105,   122,   100,    47,    86,   108,
     112,   197,   116,     6,   114,   118,   198,   199,    95,   127,
      51,   169,   170,    69,    70,   172,   100,   108,   128,   144,
     129,   178,   179,    36,   113,    71,    51,    51,    50,    29,
      51,    72,    53,    41,    62,    37,    53,   182,    38,   142,
     143,   146,    39,   133,   152,   153,   190,    54,    57,    58,
      73,   139,    64,   140,    66,    40,    59,   195,   196,    50,
      45,    51,   171,     8,     9,    52,    48,    10,    11,    12,
      13,   168,    14,    15,   181,    16,    49,   105,    17,   132,
      18,   177,    19,    20,    50,    21,    51,   160,   138,   161,
      62,   187,   144,    30,    31,    76,   150,   151,    61,    57,
      58,    50,    68,    51,   188,   189,    77,   145,    57,    58,
      89,    90,    97,   160,    91,   162,    59,    57,    58,    89,
      90,   157,   160,    91,   166,    59,    57,    58,   110,   111,
      78,    57,    58,   115,    59,    97,   101,   102,    95,    59,
      51,    42,   101,   102,    43,    11,   160,   160,   175,   176,
      33,    34,   135,   136,   163,   164,   104,    79,    83,    84,
      87,   109,   119,   120,   123,   124,   125,   155,   158,   159,
     137,   173,   185,    81,   134,   194,   141,    44,   156
};

static const yytype_uint8 yycheck[] =
{
      31,    43,    30,    34,    50,   154,     5,    53,    36,     5,
       5,     4,    53,    30,     3,     5,    33,    63,    33,     5,
     169,   170,    63,   172,    52,   116,     5,     5,    30,   178,
     179,     0,    24,   182,    62,    30,    35,    30,    30,    35,
      71,   190,    73,     4,    72,    35,   195,   196,    30,    35,
      32,   142,   143,    18,    19,   146,    35,    35,    89,    21,
      91,   152,   153,     7,    30,    30,    32,    32,    30,     3,
      32,    36,   100,    33,    36,    13,   104,   168,    13,   110,
     111,   112,     7,   100,   115,   116,   177,   104,    28,    29,
     118,   108,   109,   108,   109,     4,    36,   188,   189,    30,
      23,    32,   144,     5,     6,    36,    26,     9,    10,    11,
      12,   142,    14,    15,   160,    17,     7,   145,    20,    96,
      22,   152,    24,    25,    30,    27,    32,    35,   105,    37,
      36,   173,    21,     7,     8,     4,   113,   114,     6,    28,
      29,    30,     9,    32,   175,   176,     4,    36,    28,    29,
      30,    31,    32,    35,    34,    37,    36,    28,    29,    30,
      31,    15,    35,    34,    37,    36,    28,    29,    30,    31,
      30,    28,    29,    30,    36,    32,    28,    29,    30,    36,
      32,     6,    28,    29,     9,    10,    35,    35,    37,    37,
       7,     8,    37,    38,    28,    29,     7,    30,    30,    30,
      30,     7,    30,    30,     6,     9,    30,    10,    27,    30,
     104,    16,    21,    42,   100,    16,   109,    17,   118
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,     3,    40,    41,    30,     0,     4,    42,     5,     6,
       9,    10,    11,    12,    14,    15,    17,    20,    22,    24,
      25,    27,    43,    44,    46,    47,    48,    50,    64,     3,
       7,     8,    58,     7,     8,    61,     7,    13,    13,     7,
       4,    33,     6,     9,    64,    23,     4,    30,    26,     7,
      30,    32,    36,    51,    57,    59,    60,    28,    29,    36,
      55,     6,    36,    51,    57,    62,    63,    55,     9,    18,
      19,    30,    36,    51,    65,    66,     4,     4,    30,    30,
      45,    58,    61,    30,    30,    24,    30,    30,    49,    30,
      31,    34,    52,    53,    55,    30,    51,    32,    52,     5,
      35,    28,    29,    56,     7,    51,    52,     5,    35,     7,
      30,    31,    55,    30,    51,    30,    55,     5,    35,    30,
      30,     5,    30,     6,     9,    30,     5,    35,    55,    55,
      53,    54,    54,    57,    60,    37,    38,    59,    54,    57,
      63,    62,    55,    55,    21,    36,    55,    57,    63,    67,
      54,    54,    55,    55,    67,    10,    66,    15,    27,    30,
      35,    37,    37,    28,    29,     5,    37,     5,    55,    67,
      67,    61,    67,    16,    68,    37,    37,    55,    67,    67,
      68,    53,    67,    68,    68,    21,    68,    61,    55,    55,
      67,    68,    68,    68,    16,    67,    67,    68,    68,    68
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    39,    40,    41,    42,    42,    42,    42,    42,    42,
      42,    42,    42,    42,    42,    42,    42,    42,    42,    42,
      43,    43,    44,    45,    45,    46,    47,    47,    48,    49,
      49,    50,    51,    51,    52,    53,    53,    53,    53,    53,
      53,    54,    54,    55,    55,    55,    56,    56,    56,    56,
      57,    57,    57,    58,    58,    59,    59,    59,    59,    60,
      60,    60,    61,    61,    62,    62,    62,    62,    63,    63,
      63,    64,    65,    65,    66,    66,    66,    66,    66,    66,
      66,    66,    66,    66,    66,    66,    66,    67,    67,    67,
      67,    68,    68
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     1,     6,     0,     5,     4,     5,     4,     3,
       2,     2,     4,     5,     5,     2,     2,     2,     2,     2,
       3,     4,     5,     1,     2,     3,     3,     2,     5,     1,
       3,     2,     1,     2,     1,     2,     1,     1,     2,     1,
       1,     1,     3,     1,     1,     3,     1,     1,     3,     3,
       2,     1,     4,     5,     3,     1,     1,     3,     3,     2,
       1,     4,     5,     3,     1,     1,     3,     3,     2,     1,
       4,     5,     1,     3,     5,     6,     5,     5,     4,     6,
       5,     5,     4,     7,     7,     1,     1,     0,     1,     1,
       3,     0,     3
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* file: style_sheet  */
#line 131 "parsessh.y"
    {
      parsed_style_sheet = (yyvsp[0].sheet);
    }
#line 1468 "parsessh.c"
    break;

  case 3: /* style_sheet: tSTYLE tSTRING tIS definition_list tEND tSTYLE  */
#line 138 "parsessh.y"
      {
	(yyvsp[-2].sheet)->name = (yyvsp[-4].string);
	(yyvsp[-2].sheet)->key = "<No key yet>";
	(yyval.sheet) = (yyvsp[-2].sheet);
      }
#line 1478 "parsessh.c"
    break;

  case 4: /* definition_list: %empty  */
#line 146 "parsessh.y"
                    {
	  (yyval.sheet) = new_style_sheet ((const unsigned char *) "<no name>");
	}
#line 1486 "parsessh.c"
    break;

  case 5: /* definition_list: definition_list tOPTIONAL tKEYWORDS keywords_def tKEYWORDS  */
#line 150 "parsessh.y"
        {
	  if (highlight_level == 2) {
	    words_set_no_face ((yyvsp[-1].words), Plain_fface);
	    words_merge_rules_unique ((yyvsp[-4].sheet)->keywords, (yyvsp[-1].words));
	  }
	  (yyval.sheet) = (yyvsp[-4].sheet);
	}
#line 1498 "parsessh.c"
    break;

  case 6: /* definition_list: definition_list tKEYWORDS keywords_def tKEYWORDS  */
#line 158 "parsessh.y"
        {
	  words_set_no_face ((yyvsp[-1].words), Plain_fface);
	  words_merge_rules_unique ((yyvsp[-3].sheet)->keywords, (yyvsp[-1].words));
	  (yyval.sheet) = (yyvsp[-3].sheet);
	}
#line 1508 "parsessh.c"
    break;

  case 7: /* definition_list: definition_list tOPTIONAL tOPERATORS operators_def tOPERATORS  */
#line 164 "parsessh.y"
        {
	  if (highlight_level == 2) {
	    words_set_no_face ((yyvsp[-1].words), Plain_fface);
	    words_merge_rules_unique ((yyvsp[-4].sheet)->operators, (yyvsp[-1].words));
	  }
	  (yyval.sheet) = (yyvsp[-4].sheet);
	}
#line 1520 "parsessh.c"
    break;

  case 8: /* definition_list: definition_list tOPERATORS operators_def tOPERATORS  */
#line 172 "parsessh.y"
        {
	  words_set_no_face ((yyvsp[-1].words), Plain_fface);
	  words_merge_rules_unique ((yyvsp[-3].sheet)->operators, (yyvsp[-1].words));
	  (yyval.sheet) = (yyvsp[-3].sheet);
	}
#line 1530 "parsessh.c"
    break;

  case 9: /* definition_list: definition_list tOPTIONAL sequence_def  */
#line 177 "parsessh.y"
                                                 {
	  if (highlight_level == 2)
	    da_concat ((yyvsp[-2].sheet)->sequences, (yyvsp[0].array));
	  (yyval.sheet) = (yyvsp[-2].sheet);
	}
#line 1540 "parsessh.c"
    break;

  case 10: /* definition_list: definition_list sequence_def  */
#line 182 "parsessh.y"
                                       {
	  da_concat ((yyvsp[-1].sheet)->sequences, (yyvsp[0].array));
	  (yyval.sheet) = (yyvsp[-1].sheet);
	}
#line 1549 "parsessh.c"
    break;

  case 11: /* definition_list: definition_list ancestors_def  */
#line 186 "parsessh.y"
                                        {
	  da_concat ((yyvsp[-1].sheet)->ancestors, (yyvsp[0].array));
	  (yyval.sheet) = (yyvsp[-1].sheet);
	}
#line 1558 "parsessh.c"
    break;

  case 12: /* definition_list: definition_list tALPHABETS tARE tSTRING  */
#line 190 "parsessh.y"
                                                  {
	  string_to_array ((yyvsp[-3].sheet)->alpha1, (yyvsp[0].string));
	  string_to_array ((yyvsp[-3].sheet)->alpha2, (yyvsp[0].string));
	  (yyvsp[0].string) = NULL;
	  (yyval.sheet) = (yyvsp[-3].sheet);
	}
#line 1569 "parsessh.c"
    break;

  case 13: /* definition_list: definition_list tFIRST tALPHABET tIS tSTRING  */
#line 196 "parsessh.y"
                                                       {
	  string_to_array ((yyvsp[-4].sheet)->alpha1, (yyvsp[0].string));
	  (yyvsp[0].string) = NULL;
	  (yyval.sheet) = (yyvsp[-4].sheet);
	}
#line 1579 "parsessh.c"
    break;

  case 14: /* definition_list: definition_list tSECOND tALPHABET tIS tSTRING  */
#line 201 "parsessh.y"
                                                        {
	  string_to_array ((yyvsp[-4].sheet)->alpha2, (yyvsp[0].string));
	  (yyvsp[0].string) = NULL;
	  (yyval.sheet) = (yyvsp[-4].sheet);
	}
#line 1589 "parsessh.c"
    break;

  case 15: /* definition_list: definition_list case_def  */
#line 206 "parsessh.y"
                                   {
	  (yyvsp[-1].sheet)->sensitiveness = (yyvsp[0].sensitiveness);
	}
#line 1597 "parsessh.c"
    break;

  case 16: /* definition_list: definition_list documentation  */
#line 209 "parsessh.y"
                                        {
	  (yyvsp[-1].sheet)->documentation = (yyvsp[0].string);
	}
#line 1605 "parsessh.c"
    break;

  case 17: /* definition_list: definition_list authors  */
#line 212 "parsessh.y"
                                  {
	  (yyvsp[-1].sheet)->author = (yyvsp[0].string);
	}
#line 1613 "parsessh.c"
    break;

  case 18: /* definition_list: definition_list version  */
#line 215 "parsessh.y"
                                  {
	  style_sheet_set_version ((yyvsp[-1].sheet), (const char *) (yyvsp[0].string));
	}
#line 1621 "parsessh.c"
    break;

  case 19: /* definition_list: definition_list requirement  */
#line 218 "parsessh.y"
                                      {
	  /* Make sure now that we won't encounter new tokens.
	   * This avoids nasty error messages, or worse:
	   * unexpected behavior at run time */
	  if (!style_sheet_set_requirement ((yyvsp[-1].sheet), (const char *) (yyvsp[0].string)))
	    error (1, 0,
		   _("cannot process `%s' which requires a2ps version %s"),
		   sshfilename, (yyvsp[0].string));
	}
#line 1635 "parsessh.c"
    break;

  case 20: /* requirement: tREQUIRES tA2PS tSTRING  */
#line 233 "parsessh.y"
                                  { (yyval.string) = (yyvsp[0].string) ; }
#line 1641 "parsessh.c"
    break;

  case 21: /* requirement: tREQUIRES tA2PS tVERSION tSTRING  */
#line 234 "parsessh.y"
                                           { (yyval.string) = (yyvsp[0].string) ; }
#line 1647 "parsessh.c"
    break;

  case 22: /* documentation: tDOCUMENTATION tIS long_string tEND tDOCUMENTATION  */
#line 237 "parsessh.y"
                                                             { (yyval.string) = (yyvsp[-2].string) ; }
#line 1653 "parsessh.c"
    break;

  case 23: /* long_string: tSTRING  */
#line 239 "parsessh.y"
                     { (yyval.string) = (yyvsp[0].string); }
#line 1659 "parsessh.c"
    break;

  case 24: /* long_string: long_string tSTRING  */
#line 241 "parsessh.y"
        {
	  size_t len1;
	  size_t len2;

	  len1 = strlen ((yyvsp[-1].string));
	  (yyvsp[-1].string)[len1] = '\n';
	  len2 = strlen ((yyvsp[0].string));
	  (yyval.string) = XNMALLOC (len1 + len2 + 2, unsigned char);
	  stpcpy (stpncpy ((yyval.string), (yyvsp[-1].string), len1 + 1), (yyvsp[0].string));
	}
#line 1674 "parsessh.c"
    break;

  case 25: /* authors: tWRITTEN tBY tSTRING  */
#line 253 "parsessh.y"
                                { (yyval.string) = (yyvsp[0].string) ; }
#line 1680 "parsessh.c"
    break;

  case 26: /* version: tVERSION tIS tSTRING  */
#line 256 "parsessh.y"
                                { (yyval.string) = (yyvsp[0].string) ; }
#line 1686 "parsessh.c"
    break;

  case 27: /* version: tVERSION tSTRING  */
#line 257 "parsessh.y"
                                { (yyval.string) = (yyvsp[0].string) ; }
#line 1692 "parsessh.c"
    break;

  case 28: /* ancestors_def: tANCESTORS tARE ancestors_list tEND tANCESTORS  */
#line 263 "parsessh.y"
        {
	  /* The list of keys of style sheets from which it inherits */
  	  (yyval.array) = (yyvsp[-2].array);
	}
#line 1701 "parsessh.c"
    break;

  case 29: /* ancestors_list: tSTRING  */
#line 270 "parsessh.y"
        {
	  /* Create a list of ancestors, and drop the new one in */
	  (yyval.array) = da_new ("Ancestors tmp", 2,
		       da_linear, 2,
		       (da_print_func_t) da_str_print, NULL);
	  da_append ((yyval.array), (yyvsp[0].string));
	}
#line 1713 "parsessh.c"
    break;

  case 30: /* ancestors_list: ancestors_list ',' tSTRING  */
#line 278 "parsessh.y"
        {
	  da_append ((yyvsp[-2].array), (yyvsp[0].string));
	  (yyval.array) = (yyvsp[-2].array);
	}
#line 1722 "parsessh.c"
    break;

  case 31: /* case_def: tCASE tSENSITIVENESS  */
#line 284 "parsessh.y"
                                { (yyval.sensitiveness) = (yyvsp[0].sensitiveness) ; }
#line 1728 "parsessh.c"
    break;

  case 32: /* regex: tREGEX  */
#line 293 "parsessh.y"
        {
	  (yyval.pattern) = (yyvsp[0].pattern);
	}
#line 1736 "parsessh.c"
    break;

  case 33: /* regex: regex tREGEX  */
#line 297 "parsessh.y"
        {
	  /* Concatenate $2 to $1 makes $$ */
	  (yyval.pattern) = (yyvsp[-1].pattern);
	  (yyval.pattern)->pattern = xnrealloc ((yyval.pattern)->pattern, (yyvsp[-1].pattern)->len + (yyvsp[0].pattern)->len + 1, sizeof(char));
	  strncpy ((yyval.pattern)->pattern + (yyval.pattern)->len, (yyvsp[0].pattern)->pattern, (yyvsp[0].pattern)->len);
	  (yyval.pattern)->len += (yyvsp[0].pattern)->len;
	}
#line 1748 "parsessh.c"
    break;

  case 34: /* rhs: a_rhs  */
#line 308 "parsessh.y"
        {
	  (yyval.array) = rhs_new ();
	  rhs_add ((yyval.array), (yyvsp[0].faced_string));
	}
#line 1757 "parsessh.c"
    break;

  case 35: /* a_rhs: tSTRING fface  */
#line 316 "parsessh.y"
        {
	  (yyval.faced_string) = faced_string_new  ((yyvsp[-1].string), 0, (yyvsp[0].fface));
	}
#line 1765 "parsessh.c"
    break;

  case 36: /* a_rhs: tSTRING  */
#line 320 "parsessh.y"
        {
	  (yyval.faced_string) = faced_string_new  ((yyvsp[0].string), 0, No_fface);
	}
#line 1773 "parsessh.c"
    break;

  case 37: /* a_rhs: fface  */
#line 324 "parsessh.y"
        {
	  (yyval.faced_string) = faced_string_new  (NULL, 0, (yyvsp[0].fface));
	}
#line 1781 "parsessh.c"
    break;

  case 38: /* a_rhs: tBACK_REF fface  */
#line 328 "parsessh.y"
        {
	  (yyval.faced_string) = faced_string_new  (NULL, (yyvsp[-1].integer), (yyvsp[0].fface));
	}
#line 1789 "parsessh.c"
    break;

  case 39: /* a_rhs: tBACK_REF  */
#line 332 "parsessh.y"
        {
	  (yyval.faced_string) = faced_string_new  (NULL, (yyvsp[0].integer), No_fface);
	}
#line 1797 "parsessh.c"
    break;

  case 40: /* a_rhs: tLATEXSYMBOL  */
#line 336 "parsessh.y"
        {
	  (yyval.faced_string) = faced_string_new ((yyvsp[0].string), 0, Symbol_fface);
	}
#line 1805 "parsessh.c"
    break;

  case 41: /* rhs_list: a_rhs  */
#line 343 "parsessh.y"
        {
	  (yyval.array) = rhs_new ();
	  rhs_add ((yyval.array), (yyvsp[0].faced_string));
	}
#line 1814 "parsessh.c"
    break;

  case 42: /* rhs_list: rhs_list ',' a_rhs  */
#line 349 "parsessh.y"
        {
	  rhs_add ((yyvsp[-2].array), (yyvsp[0].faced_string));
	  (yyval.array) = (yyvsp[-2].array);
	}
#line 1823 "parsessh.c"
    break;

  case 43: /* fface: tFACE  */
#line 360 "parsessh.y"
        {
	  fface_set_face ((yyval.fface), (yyvsp[0].face));
	  fface_reset_flags ((yyval.fface));
	}
#line 1832 "parsessh.c"
    break;

  case 44: /* fface: tFFLAGS  */
#line 365 "parsessh.y"
        {
	  fface_reset_face ((yyval.fface));
	  fface_set_flags ((yyval.fface), (yyvsp[0].fflags));
	  /* If there is no face, then set Invisible */
	  fface_add_flags ((yyval.fface), ff_Invisible);
	}
#line 1843 "parsessh.c"
    break;

  case 45: /* fface: '(' fface_sxp ')'  */
#line 372 "parsessh.y"
        {
	  (yyval.fface) = (yyvsp[-1].fface);
	  /* If there is no face, then set Invisible */
	  if (fface_get_face ((yyval.fface)) == No_face)
	    fface_add_flags ((yyval.fface), ff_Invisible);
	}
#line 1854 "parsessh.c"
    break;

  case 46: /* fface_sxp: tFACE  */
#line 382 "parsessh.y"
        {
	  fface_set_face((yyval.fface), (yyvsp[0].face));
	  fface_reset_flags((yyval.fface));
	}
#line 1863 "parsessh.c"
    break;

  case 47: /* fface_sxp: tFFLAGS  */
#line 387 "parsessh.y"
        {
	  fface_reset_face((yyval.fface));
	  fface_set_flags((yyval.fface), (yyvsp[0].fflags));
	}
#line 1872 "parsessh.c"
    break;

  case 48: /* fface_sxp: fface_sxp '+' tFACE  */
#line 392 "parsessh.y"
        {
	  /* FIXME: Overloading of the face should be forbidden */
	  (yyval.fface) = (yyvsp[-2].fface);
	  fface_set_face((yyval.fface), (yyvsp[0].face));
	}
#line 1882 "parsessh.c"
    break;

  case 49: /* fface_sxp: fface_sxp '+' tFFLAGS  */
#line 398 "parsessh.y"
        {
	  (yyval.fface) = (yyvsp[-2].fface);
	  fface_add_flags((yyval.fface), (yyvsp[0].fflags));
	}
#line 1891 "parsessh.c"
    break;

  case 50: /* rule: tSTRING rhs  */
#line 412 "parsessh.y"
        {
	  (yyval.rule) = rule_new ((yyvsp[-1].string), NULL, (yyvsp[0].array),
			 sshfilename, sshlineno);
	}
#line 1900 "parsessh.c"
    break;

  case 51: /* rule: tSTRING  */
#line 417 "parsessh.y"
        {
	  (yyval.rule) = rule_new ((yyvsp[0].string), NULL,
			 rhs_new_single (NULL, 0, No_fface),
			 sshfilename, sshlineno);
	}
#line 1910 "parsessh.c"
    break;

  case 52: /* rule: '(' tSTRING rhs_list ')'  */
#line 423 "parsessh.y"
        {
	  (yyval.rule) = rule_new ((yyvsp[-2].string), NULL, (yyvsp[-1].array),
			 sshfilename, sshlineno);
	}
#line 1919 "parsessh.c"
    break;

  case 53: /* keywords_def: tIN fface tARE keywords_rule_list tEND  */
#line 434 "parsessh.y"
                                                      {
	  words_set_no_face ((yyvsp[-1].words), (yyvsp[-3].fface));
	  (yyval.words) = (yyvsp[-1].words);
	}
#line 1928 "parsessh.c"
    break;

  case 54: /* keywords_def: tARE keywords_rule_list tEND  */
#line 438 "parsessh.y"
                                       {
  	  /* First of all, the No_face must be turned into Plain */
  	  (yyval.words) = (yyvsp[-1].words);
	}
#line 1937 "parsessh.c"
    break;

  case 55: /* keywords_rule_list: rule  */
#line 446 "parsessh.y"
        {
	  (yyval.words) = words_new ("Keywords: Strings", "Keywords: Regexps", 100, 100);
	  words_add_string ((yyval.words), (yyvsp[0].rule));
	}
#line 1946 "parsessh.c"
    break;

  case 56: /* keywords_rule_list: keyword_regex  */
#line 451 "parsessh.y"
        {
	  (yyval.words) = words_new ("Keywords: Strings", "Keywords: Regexps", 100, 100);
	  words_add_regex ((yyval.words), (yyvsp[0].rule));
	}
#line 1955 "parsessh.c"
    break;

  case 57: /* keywords_rule_list: keywords_rule_list ',' rule  */
#line 456 "parsessh.y"
        {
	  words_add_string ((yyvsp[-2].words), (yyvsp[0].rule));
	  (yyval.words) = (yyvsp[-2].words);
	}
#line 1964 "parsessh.c"
    break;

  case 58: /* keywords_rule_list: keywords_rule_list ',' keyword_regex  */
#line 461 "parsessh.y"
        {
	  words_add_regex ((yyvsp[-2].words), (yyvsp[0].rule));
	  (yyval.words) = (yyvsp[-2].words);
	}
#line 1973 "parsessh.c"
    break;

  case 59: /* keyword_regex: regex rhs  */
#line 473 "parsessh.y"
        {
	  (yyval.rule) = keyword_rule_new (NULL, (yyvsp[-1].pattern), (yyvsp[0].array),
				 sshfilename, sshlineno);
	}
#line 1982 "parsessh.c"
    break;

  case 60: /* keyword_regex: regex  */
#line 478 "parsessh.y"
        {
	  (yyval.rule) = keyword_rule_new (NULL, (yyvsp[0].pattern),
				   rhs_new_single (NULL, 0,
							    No_fface),
				 sshfilename, sshlineno);
	}
#line 1993 "parsessh.c"
    break;

  case 61: /* keyword_regex: '(' regex rhs_list ')'  */
#line 485 "parsessh.y"
        {
	  (yyval.rule) = keyword_rule_new (NULL, (yyvsp[-2].pattern), (yyvsp[-1].array),
				 sshfilename, sshlineno);
	}
#line 2002 "parsessh.c"
    break;

  case 62: /* operators_def: tIN fface tARE operators_rule_list tEND  */
#line 494 "parsessh.y"
                                                       {
	  words_set_no_face ((yyvsp[-1].words), (yyvsp[-3].fface));
	  (yyval.words) = (yyvsp[-1].words);
	}
#line 2011 "parsessh.c"
    break;

  case 63: /* operators_def: tARE operators_rule_list tEND  */
#line 498 "parsessh.y"
                                        {
  	  /* First of all, the No_face must be turned into Plain */
  	  (yyval.words) = (yyvsp[-1].words);
	}
#line 2020 "parsessh.c"
    break;

  case 64: /* operators_rule_list: rule  */
#line 506 "parsessh.y"
        {
	  (yyval.words) = words_new ("Operators: Strings", "Operators: Regexps",
			  100, 100);
	  words_add_string ((yyval.words), (yyvsp[0].rule));
	}
#line 2030 "parsessh.c"
    break;

  case 65: /* operators_rule_list: operator_regex  */
#line 512 "parsessh.y"
        {
	  (yyval.words) = words_new ("Operators: Strings", "Operators: Regexps",
			  100, 100);
	  words_add_regex ((yyval.words), (yyvsp[0].rule));
	}
#line 2040 "parsessh.c"
    break;

  case 66: /* operators_rule_list: operators_rule_list ',' rule  */
#line 518 "parsessh.y"
        {
	  words_add_string ((yyvsp[-2].words), (yyvsp[0].rule));
	  (yyval.words) = (yyvsp[-2].words);
	}
#line 2049 "parsessh.c"
    break;

  case 67: /* operators_rule_list: operators_rule_list ',' operator_regex  */
#line 523 "parsessh.y"
        {
	  words_add_regex ((yyvsp[-2].words), (yyvsp[0].rule));
	  (yyval.words) = (yyvsp[-2].words);
	}
#line 2058 "parsessh.c"
    break;

  case 68: /* operator_regex: regex rhs  */
#line 535 "parsessh.y"
        {
	  (yyval.rule) = rule_new (NULL, (yyvsp[-1].pattern), (yyvsp[0].array),
			 sshfilename, sshlineno);
	}
#line 2067 "parsessh.c"
    break;

  case 69: /* operator_regex: regex  */
#line 540 "parsessh.y"
        {
	  (yyval.rule) = rule_new (NULL, (yyvsp[0].pattern),
			   rhs_new_single (NULL, 0, No_fface),
			 sshfilename, sshlineno);
	}
#line 2077 "parsessh.c"
    break;

  case 70: /* operator_regex: '(' regex rhs_list ')'  */
#line 546 "parsessh.y"
        {
	  (yyval.rule) = rule_new (NULL, (yyvsp[-2].pattern), (yyvsp[-1].array),
			 sshfilename, sshlineno);
	}
#line 2086 "parsessh.c"
    break;

  case 71: /* sequence_def: tSEQUENCES tARE sequence_list tEND tSEQUENCES  */
#line 555 "parsessh.y"
                                                             { (yyval.array) = (yyvsp[-2].array); }
#line 2092 "parsessh.c"
    break;

  case 72: /* sequence_list: sequence  */
#line 558 "parsessh.y"
                        {
	  (yyval.array) = da_new ("Sequence tmp", 100,
		       da_linear, 100,
		       (da_print_func_t) sequence_self_print, NULL);
	  da_append ((yyval.array), (yyvsp[0].sequence));
	}
#line 2103 "parsessh.c"
    break;

  case 73: /* sequence_list: sequence_list ',' sequence  */
#line 564 "parsessh.y"
                                     {
	  da_append ((yyvsp[-2].array), (yyvsp[0].sequence));
	  (yyval.array) = (yyvsp[-2].array);
	}
#line 2112 "parsessh.c"
    break;

  case 74: /* sequence: tSTRING tLATEXSYMBOL fface closers_opt exception_def_opt  */
#line 588 "parsessh.y"
        {
	  struct rule * open_rule;
	  open_rule = rule_new ((yyvsp[-4].string), NULL,
				rhs_new_single ((yyvsp[-3].string), 0, Symbol_fface),
				sshfilename, sshlineno);
	  (yyval.sequence) = sequence_new (open_rule, (yyvsp[-2].fface), (yyvsp[-1].words), (yyvsp[0].words));
	}
#line 2124 "parsessh.c"
    break;

  case 75: /* sequence: tSTRING tSTRING fface fface closers_opt exception_def_opt  */
#line 596 "parsessh.y"
        {
	  struct rule * open_rule;
	  open_rule = rule_new ((yyvsp[-5].string), NULL,
				rhs_new_single ((yyvsp[-4].string), 0, (yyvsp[-3].fface)),
				sshfilename, sshlineno);
	  (yyval.sequence) = sequence_new (open_rule, (yyvsp[-2].fface), (yyvsp[-1].words), (yyvsp[0].words));
	}
#line 2136 "parsessh.c"
    break;

  case 76: /* sequence: tSTRING fface fface closers_opt exception_def_opt  */
#line 604 "parsessh.y"
        {
	  struct rule * open_rule;
	  open_rule = rule_new ((yyvsp[-4].string), NULL,
				rhs_new_single (NULL, 0, (yyvsp[-3].fface)),
				sshfilename, sshlineno);
	  (yyval.sequence) = sequence_new (open_rule, (yyvsp[-2].fface), (yyvsp[-1].words), (yyvsp[0].words));
	}
#line 2148 "parsessh.c"
    break;

  case 77: /* sequence: tSTRING tSTRING fface closers_opt exception_def_opt  */
#line 612 "parsessh.y"
        {
	  struct rule * open_rule;
	  open_rule = rule_new ((yyvsp[-4].string), NULL,
				rhs_new_single ((yyvsp[-3].string), 0, (yyvsp[-2].fface)),
				sshfilename, sshlineno);
	  (yyval.sequence) = sequence_new (open_rule, (yyvsp[-2].fface), (yyvsp[-1].words), (yyvsp[0].words));
	}
#line 2160 "parsessh.c"
    break;

  case 78: /* sequence: tSTRING fface closers_opt exception_def_opt  */
#line 620 "parsessh.y"
        {
	  struct rule * open_rule;
	  open_rule = rule_new ((yyvsp[-3].string), NULL,
				rhs_new_single (NULL, 0, (yyvsp[-2].fface)),
				sshfilename, sshlineno);
	  (yyval.sequence) = sequence_new (open_rule, (yyvsp[-2].fface), (yyvsp[-1].words), (yyvsp[0].words));
	}
#line 2172 "parsessh.c"
    break;

  case 79: /* sequence: regex tSTRING fface fface closers_opt exception_def_opt  */
#line 628 "parsessh.y"
        {
	  struct rule * open_rule;
	  open_rule = rule_new (NULL, (yyvsp[-5].pattern),
				rhs_new_single ((yyvsp[-4].string), 0, (yyvsp[-3].fface)),
				sshfilename, sshlineno);
	  (yyval.sequence) = sequence_new (open_rule, (yyvsp[-2].fface), (yyvsp[-1].words), (yyvsp[0].words));
	}
#line 2184 "parsessh.c"
    break;

  case 80: /* sequence: regex fface fface closers_opt exception_def_opt  */
#line 636 "parsessh.y"
        {
	  struct rule * open_rule;
	  open_rule = rule_new (NULL, (yyvsp[-4].pattern),
				rhs_new_single (NULL, 0, (yyvsp[-3].fface)),
				sshfilename, sshlineno);
	  (yyval.sequence) = sequence_new (open_rule, (yyvsp[-2].fface), (yyvsp[-1].words), (yyvsp[0].words));
	}
#line 2196 "parsessh.c"
    break;

  case 81: /* sequence: regex tSTRING fface closers_opt exception_def_opt  */
#line 644 "parsessh.y"
        {
	  struct rule * open_rule;
	  open_rule = rule_new (NULL, (yyvsp[-4].pattern),
				rhs_new_single ((yyvsp[-3].string), 0, (yyvsp[-2].fface)),
				sshfilename, sshlineno);
	  (yyval.sequence) = sequence_new (open_rule, (yyvsp[-2].fface), (yyvsp[-1].words), (yyvsp[0].words));
	}
#line 2208 "parsessh.c"
    break;

  case 82: /* sequence: regex fface closers_opt exception_def_opt  */
#line 652 "parsessh.y"
        {
	  struct rule * open_rule;
	  open_rule = rule_new (NULL, (yyvsp[-3].pattern),
				rhs_new_single (NULL, 0, No_fface),
				sshfilename, sshlineno);
	  (yyval.sequence) = sequence_new (open_rule, (yyvsp[-2].fface), (yyvsp[-1].words), (yyvsp[0].words));
	}
#line 2220 "parsessh.c"
    break;

  case 83: /* sequence: '(' tSTRING rhs_list ')' fface closers_opt exception_def_opt  */
#line 660 "parsessh.y"
        {
	  struct rule * open_rule;
	  open_rule = rule_new ((yyvsp[-5].string), NULL, (yyvsp[-4].array),
				sshfilename, sshlineno);
	  (yyval.sequence) = sequence_new (open_rule, (yyvsp[-2].fface), (yyvsp[-1].words), (yyvsp[0].words));
	}
#line 2231 "parsessh.c"
    break;

  case 84: /* sequence: '(' regex rhs_list ')' fface closers_opt exception_def_opt  */
#line 667 "parsessh.y"
        {
	  struct rule * open_rule;
	  open_rule = rule_new (NULL, (yyvsp[-5].pattern), (yyvsp[-4].array),
				sshfilename, sshlineno);
	  (yyval.sequence) = sequence_new (open_rule, (yyvsp[-2].fface), (yyvsp[-1].words), (yyvsp[0].words));
	}
#line 2242 "parsessh.c"
    break;

  case 85: /* sequence: tCSTRING  */
#line 675 "parsessh.y"
        {
	  (yyval.sequence) = new_C_string_sequence ("\"");
	}
#line 2250 "parsessh.c"
    break;

  case 86: /* sequence: tCCHAR  */
#line 679 "parsessh.y"
        {
	  (yyval.sequence) = new_C_string_sequence ("\'");
	}
#line 2258 "parsessh.c"
    break;

  case 87: /* closers_opt: %empty  */
#line 686 "parsessh.y"
        {
	  /* This is a shortcut which means "up to the end of the line". */
	  (yyval.words) = words_new ("Closing: Strings", "Closing: Regexps", 2, 2);
	  words_add_string ((yyval.words), rule_new (xstrdup ("\n"), NULL,
					  rhs_new_single (NULL, 0,
							  No_fface),
					  sshfilename, sshlineno));
	}
#line 2271 "parsessh.c"
    break;

  case 88: /* closers_opt: rule  */
#line 695 "parsessh.y"
        {
	  /* Only one */
	  (yyval.words) = words_new ("Closing: Strings", "Closing: Regexps", 2, 2);
	  words_add_string ((yyval.words), (yyvsp[0].rule));
	}
#line 2281 "parsessh.c"
    break;

  case 89: /* closers_opt: operator_regex  */
#line 701 "parsessh.y"
        {
	  /* Only one */
	  (yyval.words) = words_new ("Closing: Strings", "Closing: Regexps", 2, 2);
	  words_add_regex ((yyval.words), (yyvsp[0].rule));
	}
#line 2291 "parsessh.c"
    break;

  case 90: /* closers_opt: tCLOSERS operators_def tCLOSERS  */
#line 707 "parsessh.y"
        {
	  /* Several, comma separated, between () */
	  (yyval.words) = (yyvsp[-1].words);
	}
#line 2300 "parsessh.c"
    break;

  case 91: /* exception_def_opt: %empty  */
#line 715 "parsessh.y"
        {
	  (yyval.words) = words_new ("Exceptions: Strings", "Exceptions: Regexps", 1, 1);
	}
#line 2308 "parsessh.c"
    break;

  case 92: /* exception_def_opt: tEXCEPTIONS operators_def tEXCEPTIONS  */
#line 719 "parsessh.y"
        {
	  (yyval.words) = (yyvsp[-1].words);
	}
#line 2316 "parsessh.c"
    break;


#line 2320 "parsessh.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 723 "parsessh.y"


void
yyerror (const char *msg)
{
  error_at_line (1, 0, sshfilename, sshlineno, "%s", msg);
}

/*
 * FIXME: Cover the other relevant types
 */
static void
yyprint (FILE *file, int type, YYSTYPE value)
{
  switch (type) {
  case tBACK_REF:
    fprintf (file, " \\%d", value.integer);
    break;

  case tFFLAGS:
    putc (' ', file);
    fflag_self_print (value.fflags, file);
    break;

  case tFACE:
    fprintf (file, " %s", face_to_string (value.face));
    break;

  case tREGEX:
    fprintf (file, " /%s/", value.pattern->pattern);
    break;

  case tSTRING:
    fprintf (file, " \"%s\"", value.string);
    break;
  }
}

struct style_sheet *
parse_style_sheet (const char * filename)
{
  int res;

  sshfilename = filename;
  sshlineno = 1;
  sshin = xrfopen (sshfilename);

  message (msg_file | msg_sheet | msg_parse,
	   (stderr, "Parsing file `%s'\n", sshfilename));

  sshlex_initialize ();

  if (msg_test (msg_parse))
    yydebug = true;
  res = yyparse ();  /* FIXME: test the result of parsing */

  if (msg_test (msg_sheet)) {
    fprintf (stderr, "---------- Right after parsing of %s\n",
	     parsed_style_sheet->key);
    style_sheet_self_print (parsed_style_sheet, stderr);
    fprintf (stderr, "---------- End of after parsing of %s\n",
	     parsed_style_sheet->key);
  }

  fclose (sshin);
  return parsed_style_sheet;
}
