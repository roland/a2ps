/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_PARSESSH_H_INCLUDED
# define YY_YY_PARSESSH_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    tSTYLE = 258,                  /* tSTYLE  */
    tIS = 259,                     /* tIS  */
    tEND = 260,                    /* tEND  */
    tKEYWORDS = 261,               /* tKEYWORDS  */
    tARE = 262,                    /* tARE  */
    tIN = 263,                     /* tIN  */
    tOPERATORS = 264,              /* tOPERATORS  */
    tSEQUENCES = 265,              /* tSEQUENCES  */
    tFIRST = 266,                  /* tFIRST  */
    tSECOND = 267,                 /* tSECOND  */
    tALPHABET = 268,               /* tALPHABET  */
    tALPHABETS = 269,              /* tALPHABETS  */
    tDOCUMENTATION = 270,          /* tDOCUMENTATION  */
    tEXCEPTIONS = 271,             /* tEXCEPTIONS  */
    tCASE = 272,                   /* tCASE  */
    tCSTRING = 273,                /* tCSTRING  */
    tCCHAR = 274,                  /* tCCHAR  */
    tOPTIONAL = 275,               /* tOPTIONAL  */
    tCLOSERS = 276,                /* tCLOSERS  */
    tWRITTEN = 277,                /* tWRITTEN  */
    tBY = 278,                     /* tBY  */
    tVERSION = 279,                /* tVERSION  */
    tREQUIRES = 280,               /* tREQUIRES  */
    tA2PS = 281,                   /* tA2PS  */
    tANCESTORS = 282,              /* tANCESTORS  */
    tFACE = 283,                   /* tFACE  */
    tFFLAGS = 284,                 /* tFFLAGS  */
    tSTRING = 285,                 /* tSTRING  */
    tLATEXSYMBOL = 286,            /* tLATEXSYMBOL  */
    tREGEX = 287,                  /* tREGEX  */
    tSENSITIVENESS = 288,          /* tSENSITIVENESS  */
    tBACK_REF = 289                /* tBACK_REF  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define tSTYLE 258
#define tIS 259
#define tEND 260
#define tKEYWORDS 261
#define tARE 262
#define tIN 263
#define tOPERATORS 264
#define tSEQUENCES 265
#define tFIRST 266
#define tSECOND 267
#define tALPHABET 268
#define tALPHABETS 269
#define tDOCUMENTATION 270
#define tEXCEPTIONS 271
#define tCASE 272
#define tCSTRING 273
#define tCCHAR 274
#define tOPTIONAL 275
#define tCLOSERS 276
#define tWRITTEN 277
#define tBY 278
#define tVERSION 279
#define tREQUIRES 280
#define tA2PS 281
#define tANCESTORS 282
#define tFACE 283
#define tFFLAGS 284
#define tSTRING 285
#define tLATEXSYMBOL 286
#define tREGEX 287
#define tSENSITIVENESS 288
#define tBACK_REF 289

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 82 "parsessh.y"

  int integer;
  char * string;
  struct pattern * pattern;
  struct style_sheet * sheet;
  struct rule * rule;
  struct sequence * sequence;
  struct darray * array;
  struct words * words;
  struct faced_string * faced_string;
  enum face_e face;			/* Face			*/
  enum fflag_e fflags;			/* Flags for faces	*/
  struct fface_s fface;			/* Flagged face		*/
  enum case_sensitiveness sensitiveness;

#line 151 "parsessh.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;


int yyparse (void);


#endif /* !YY_YY_PARSESSH_H_INCLUDED  */
